module github.com/visheshc14/Neo-Go

go 1.17

require (
	github.com/klauspost/compress v1.11.12 // indirect
	github.com/klauspost/cpuid v1.2.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/valyala/fasthttp v1.22.0 // indirect
)
